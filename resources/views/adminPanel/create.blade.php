@extends('layouts.template')

@section('title', 'Add New Post')

@section('content')


<div class="container">
    
    <form action="{{ route('posts.store') }}" method="POST" role="form">
        {{ csrf_field() }}
        <legend>Add New Post</legend>
    
        <div class="form-group">
            <label for="">Title:</label>
            <input type="text" class="form-control" name="title" id="" placeholder="Input field">
        </div>

        <div class="form-group">
            <label for="">Content:</label>
            <textarea class="form-control" name="body" id="" placeholder="Input field"></textarea>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
        
        <a class="btn btn-warning" href="{{ route('posts.index')}}" role="button">Go back</a>
        
    </form>
    
</div>


@endsection()