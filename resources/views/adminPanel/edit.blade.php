@extends('layouts.template')

@section('title', 'Edit Post #' . $post->id)

@section('content')


<div class="container">
    
    <form action="{{ route('posts.update', ['id'=>$post->id]) }}" method="POST" role="form">
        {{ csrf_field() }}
        <input type="hidden" name="_method" value="PUT">
        
        <legend>Edit Post # {{ $post->id }}</legend>
    
        <div class="form-group">
            <label for="">Title:</label>
            <input type="text" class="form-control" name="title" id="" placeholder="Input field" value="{{ $post->title }}">
        </div>

        <div class="form-group">
            <label for="">Content:</label>
            <textarea class="form-control" name="body" id="" placeholder="Input field">{{ $post->body }}</textarea>
        </div>

        <input type="hidden" name="editForm" value="editForm">
        <button type="submit" class="btn btn-primary">Submit</button>
        
        <a class="btn btn-warning" href="{{ route('posts.index')}}" role="button">Go back</a>
        
    </form>
    
</div>


@endsection()