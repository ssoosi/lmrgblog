@extends('layouts.template')

@section('title', 'Manage Post Admin')


@section('content')

<div class="container">
  <h1 class="m-3">Admin Panel</h1>
    <a href="{{ route('posts.create') }}" class="btn btn-sm btn-primary float-right m-3">Add new blog post</a><br>
<div class="table-responsive">
    <table class="table table-hover">
        <thead>
            <tr>
                <th>Id</th>
                <th>Title</th>
                <th>Content</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>
        </thead>
        <tbody>
        @foreach($posts as $post)
            <tr>
                <td>{{ $post->id  }}</td>
                <td>{{ $post->title  }}</td>
                <td>{{ $post->body  }}</td>
                <td>
                   <a class="btn btn-sm btn-warning" href="{{ route('posts.edit',['id'=>$post->id]) }}" role="button">Edit Post</a>    
                </td>
                <td>
                    <form action="{{ route('posts.destroy', ['id'=>$post->id]) }}" method="post">
                    {{ csrf_field() }}
                        <input type="hidden" name="_method" value="DELETE">
                        <input class="btn btn-sm btn-danger" type="submit" value="Delete">
                    </form>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
</div>
@endsection