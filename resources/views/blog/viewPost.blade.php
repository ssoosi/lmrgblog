@extends('layouts.publicHomePageTemplate')

@section('title', 'View Post #' . $id)
  
@section('content')   

<div class="container">
    <h2>{{ $post->title  }}</h2>
    <h6><strong>Post Created At:</strong> {{ date('F d, Y', strtotime($post->created_at)) }}</h6>
    <p>{{ $post->body }}</p>    
    <a class="btn btn-sm btn-primary" href="{{ url('/') }}" role="button">Back to home</a>
       
</div>

    
@endsection