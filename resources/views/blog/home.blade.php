@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>The LRMG Blog</h1>
        <div class="row">  
            @foreach($posts as $post)
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="p-3 mb-2 bg-primary text-white">
                            <h2>{{ $post->title }}</h2>
                            <hr>
                            <h6><strong>Post Created At:</strong> {{ date('F d, Y', strtotime($post->created_at)) }}</h6>
                            <p>{{ $post->body }}</p>
                            <a class="btn btn-sm btn-warning" href="{{ route('posts.show',['id'=>$post->id]) }}" role="button">View all</a>    
                        </div>
                </div>
            @endforeach    
        
        </div>
        <div class="text-center">{{ $posts->links() }}</div>
    </div>
@endsection
