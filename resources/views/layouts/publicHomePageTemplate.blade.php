@extends('layouts.app')

@section('content')
<div class="container">
  <h1>@yield('title')</h1>
  
  <div class="row">
      
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="p-3 mb-2 bg-dark text-white">
                <h2>Post 1</h2>
                <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Incidunt dicta dignissimos nam inventore aut? Facere dolor voluptatum magni deleniti nulla quasi laboriosam animi et officia repellat sunt enim, aperiam recusandae.</p>     
            </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="p-3 mb-2 bg-dark text-white">
                <h2>Post 2</h2>
                <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Incidunt dicta dignissimos nam inventore aut? Facere dolor voluptatum magni deleniti nulla quasi laboriosam animi et officia repellat sunt enim, aperiam recusandae.</p>     
            </div>
      </div>
      
  </div>
  
</div>
@endsection
