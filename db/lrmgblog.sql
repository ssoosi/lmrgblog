-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 18, 2018 at 08:08 AM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lrmgblog`
--

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_05_17_100626_create_posts_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `user_id`, `title`, `body`, `created_at`, `updated_at`) VALUES
(3, 2, 'Post 3', 'fsdfksdnmfksdmlkfmsklfmsd', '2018-05-17 18:18:46', '2018-05-17 18:18:46'),
(4, 2, 'fdsfsdfsdfsdf', 'sfdsdfsdfdsfsdsdsdsdsdsdsdsdsdsdsdsdsdsdsdsdsdsdsdsdsdsdsdsd', '2018-05-17 18:18:55', '2018-05-17 18:18:55'),
(5, 2, 'FDFDF', 'gdfgdfgdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdf', '2018-05-17 18:19:07', '2018-05-17 18:19:07'),
(6, 2, 'gfgf', 'gdfgdddddddddddddddddddddddddddddddddddddddddddf', '2018-05-17 18:19:21', '2018-05-17 18:19:21'),
(7, 2, 'hhghfhfhgfhgfhgfhgf', 'hgfhgfgfgfgfgfgfgfgfgfgfgfgfgfgfgfgfgfgfgfgfgfgfgfgfgfgfgfgfgfgfgfgfgfgf', '2018-05-17 18:19:31', '2018-05-17 18:19:31'),
(8, 2, 'dsdddsdsdsdsd', 'dsdsdsdsdssssssssssssssssssssssssssssssssssssssssss', '2018-05-17 19:55:55', '2018-05-17 19:55:55'),
(12, 1, 'ggdfgdf', 'ggdfgdf', '2018-05-18 04:04:16', '2018-05-18 04:04:16');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Sam Tabriz', 'ssoosi@gmail.com', '$2y$10$8VhEYE3pyz7ktQQ0yCBKlOi8YvqYNhqVtWkjCbU8zzwdF9TSchw1S', 'jECoteKjdOJF7tNwRTP9jD2VrPkSzWiKrSfGr8Cy2Zfq0aKckgLZJzckjF2n', '2018-05-17 17:16:26', '2018-05-17 17:16:26'),
(2, 'zaan', 'zaaanfki@gmail.com', '$2y$10$t2atL3o/DqdX2ntNT6IXfeHujTwkfGZGeP4gU11PBb0U91mEW.Dxa', 'VoLSnebyYQLYKv9L9oVsZIctBryCunEu3Khlr3x3xqxXWsGzLQU0l04ot8z0', '2018-05-17 18:18:14', '2018-05-17 18:18:14'),
(3, 'vadasdsddas', 'ssoodddsi@gmail.com', '$2y$10$OgqZpAFb3VlfwSFoQWNW6eGRK2YLg9f9wYAJE9/1OcgyTKcgZdpqm', 'vAFOd8l1SoTXewlw6u9CYNV9qFuQ1ic1i2KuhRodNc0zc1owiZRJrLvM4rGd', '2018-05-17 22:13:02', '2018-05-17 22:13:02'),
(4, 'Zaan', 'zaa@gmail.co', '$2y$10$0tQa/EV0LAGTGpNhvmy8C.RYGAwFFoBfLmklkaS7i7g5sVLKAMrX2', 'Ovm2rSKEeZX6xbiQOyi6ajIuz5Be94aPYAxdx7i09uB7Uf7VxPcSkwvR2FLo', '2018-05-18 02:25:56', '2018-05-18 02:25:56'),
(5, 'John', 'info@healthsystems.co.za', '$2y$10$Wb6rQhAdhRRR8mFPinxrfexeRF0Jlriz49tx1jNlIJbX.SY1WO2Qi', '9e2n3ss6nkT0NNhMH7bnvKZCO8fbgRMy8KigQDq9Kz3arSwdSnsISlK5Wu53', '2018-05-18 02:31:55', '2018-05-18 02:31:55'),
(6, 'wwwwswswswsw', 'info@healtdddhsystems.co.za', '$2y$10$k1GIbHeOY7S1ulOaaB7ZP.JLnUM5wLAZLNyhK2zx3q3jl8znEdqu2', 'wPaFkYa9IohNMz215zSaTn4uYHQSFO6dadIwySzAFGfxLRRwp7B1Bxj2Sbse', '2018-05-18 02:34:01', '2018-05-18 02:34:01');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
